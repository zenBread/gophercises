package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"math/rand"
	"os"
	"time"
)

func main() {

	csvFilename := flag.String("csv", "problems.csv", "a csv file `string` in the format of (question, answer)")
	timeLimit := flag.Int("limit", 30, "Enter number in seconds for the timer on the quiz.")
	var shuffle bool
	flag.BoolVar(&shuffle, "shuffle", false, "Set `bool` to shuffle questions. (Default false)")
	flag.Parse()

	file, err := os.Open(*csvFilename)
	if err != nil {
		exit(fmt.Sprintf("Failed to open the CSV file: %s", *csvFilename))
	}

	r := csv.NewReader(file)
	lines, err := r.ReadAll()
	if err != nil {
		exit("Failed to parse the provided csv file.")
	}
	problems := parseLines(lines)
	if shuffle {
		problems = shuffleProblems(&problems)
	}

	timer := time.NewTimer(time.Duration(*timeLimit) * time.Second)
	correct := 0

problemLoop:
	for i, p := range problems {
		fmt.Printf("Problem #%d: %s = ", i+1, p.q)
		answerCh := make(chan string)
		go func() {
			var answer string
			fmt.Scanf("%s\n", &answer)
			answerCh <- answer
		}()
		select {
		case <-timer.C:
			fmt.Println()
			break problemLoop
		case answer := <-answerCh:
			if answer == p.a {
				correct++
			}
		}
	}
	fmt.Printf("You got %d out of %d\n", correct, len(problems))

}

// Pulls csv file lines and places them into a problem type
func parseLines(lines [][]string) []problem {
	ret := make([]problem, len(lines))
	for x, line := range lines {
		ret[x] = problem{
			q: line[0],
			a: line[1],
		}
	}

	return ret
}

// shuffleProblems returns a random ordered problem type slice
// Param: type problem slice
// Returns: random ordered type problem slice
func shuffleProblems(p *[]problem) []problem {
	rand.Seed(time.Now().UTC().UnixNano())
	dest := make([]problem, len(*p))
	perm := rand.Perm(len(*p))
	for i, v := range perm {
		dest[v] = (*p)[i]
	}
	return dest
}

type problem struct {
	q string
	a string
}

// exit prints a message on error and exits the program
func exit(msg string) {
	fmt.Println(msg)
	os.Exit(1)
}
